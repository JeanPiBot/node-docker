module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {
    'no-console': 'off',
    'padded-blocks': 'off',
    'arrow-body-style': 'off',
    'no-restricted-syntax': 'off',
    'no-useless-escape': 'off',
    'no-plusplus': 'off',
  },
};
