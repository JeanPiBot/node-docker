# Node-docker

Learning about docker.

# Step by step

Firstly, create your app with node and express.

Secondly, create Dockerfile.

After, create .dockerignore.

Finally, use the next commands:

# build image in docker

```
docker build -t node-app-image .
```
__NOTE:__ `.` it is current folder.

# run in docker

__NOTE:__ `$(pwd)` Depends on your computer, it is mac and unix you can use this one, despite, it is window you use the next one `%cd%`  and `/app` depends on what name you app in dockerfile.

```
docker run -v $(pwd):/app -p 3000:3000 -d --name node-app node-app-image
```

__NOTE:__  When you delete node_modules in your current folder into the volumen you can use the next hack for solve this problem.

```
docker run -v $(pwd):/app -v /app/node_modules -p 3000:3000 -d --name node-app node-app-image
```

__NOTE:__  If you want to protect your folder in docker for example app you can do that with this command at end the 'app' which means read only

```
docker run -v $(pwd):/app:ro -v /app/node_modules -p 3000:3000 -d --name node-app node-app-image
```

__NOTE:__  If you want to pass env varibles in docker you can do with the next command:

```
docker run -v $(pwd):/app:ro -v /app/node_modules --env-file ./.env -p 3000:4000 -d --name node-app node-app-image
```

# Execute in docker

```
docker exec -it node-app bash
```

# List volume in docker

```
docker volume ls
```

# Delete volume in docker

```
docker volume rm 2ac3a4f1b65cee111378d7a7e67788f3aefca4381958ccd0a95c67ad590f1a4f
```

Other way to do that is 

```
docker volume prune
```
__NOTE:__  Becareful with that command, because delete all volume in the system. Better use the next command.

```
docker rm node-app -fv
```

# More information

[FreeCodeCamp.org](https://www.youtube.com/watch?v=9zUHg7xjIqQ)

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

# Authors

- **Jean Pierre Giovanni Arenas Ortiz**

# License

[MIT](https://choosealicense.com/licenses/mit/)