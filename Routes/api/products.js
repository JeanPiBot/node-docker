const express = require('express');

const router = express.Router();

const { rethinkdb, desc } = require('../../services/rethinkdb/rethinkdb');

router.get('/', async (req, res) => {

  try {
    const products = await rethinkdb.table('products').orderBy(desc).run(req.rdb);

    res.status(200).json({
      data: products,
      message: 'product listed',
    });
  } catch (error) {
    res.status(400).json(error);
  }

});

router.get('/:productId', async (req, res) => {
  const { productId } = req.params;

  try {
    const product = await rethinkdb.table('products').get(productId).run(req.rdb);

    res.status(200).json({
      data: product,
      message: 'product retrieved',
    });
  } catch (error) {
    res.status(400).json(error);
  }

});

router.post('/', async (req, res) => {

  try {
    const datos = req.body;

    const product = await rethinkdb.table('products').insert(datos).run(req.rdb);

    res.status(201).json({
      data: product,
      message: 'product created',
    });
  } catch (error) {
    res.status(400).json(error);
  }

});

router.put('/:productId', async (req, res) => {
  const { productId } = req.params;
  const { body } = req;

  try {
    const product = await rethinkdb.table('products').get(productId).update(body).run(req.rdb);

    res.status(200).json({
      data: product,
      message: 'product updated',
    });
  } catch (error) {
    res.status(400).json(error);
  }

});

router.delete('/:productId', async (req, res) => {
  const { productId } = req.params;

  try {
    const product = await rethinkdb.table('books').get(productId).delete().run(req.rdb);

    res.status(200).json({
      data: product,
      message: 'product removed',
    });
  } catch (error) {
    res.status(400).json(error);
  }

});

module.exports = router;
